#pragma once
#include <string>
#include <iostream>
using namespace std;

class CongTrinhKhoaHoc
{
private:
	int MaCT;
	int NamXuatBan;
	string TenCTKH;
	bool KhuVuc;
public:
	CongTrinhKhoaHoc();
	CongTrinhKhoaHoc(int, int, string, bool);
	CongTrinhKhoaHoc(const CongTrinhKhoaHoc&);
	~CongTrinhKhoaHoc();
	friend int getID(const CongTrinhKhoaHoc&);
	void Show();
};

