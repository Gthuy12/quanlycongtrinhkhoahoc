#include <iostream>
#include "DanhSachCongTrinh.h"
using namespace std;

bool tang(int left, int right) {
	if (left < right) {
		return true;
	}
	else {
		return false;
	}
};

bool giam(int left, int right) {
	if (left > right) {
		return true;
	}
	else {
		return false;
	}
};

int main()
{
	DanhSachCongTrinh p;
	int choice1,choice2,choice3;
	do
	{
		system("cls");
		cout << "-------- MAIN MENU ---------" << endl;
		cout << "1. Xem danh sach cong trinh" << endl;
		cout << "2. Them cong trinh moi" << endl;
		cout << "3. Xoa cong trinh " << endl;
		cout << "4. Cap nhat cong trinh" << endl;
		cout << "5. Tim kiem cong trinh (theo ma cong trinh) " << endl;
		cout << "6. Sap xep cong trinh (theo ma cong trinh )" << endl;
		cout << "0. Thoat" << endl;
		cout << " Nhap vao lua chon: ";
		cin >> choice1;
		cin.ignore();
		switch (choice1)
		{
		case 1:
			system("cls");
			p.Display();
			cout << "Bam 0 de tro lai Main menu " << endl;
			cin >> choice2;
			while (choice2 != 0)
			{
				cout << "Ban phai nhap 0 de tro lai Main menu " << endl;
				cout << "Nhap lai: ";
				cin >> choice2;					
			}
			break;
		case 2:
			system("cls");
			do {
				cout << "------- Menu tao --------" << endl;
				cout << "1. Them dau danh sach" << endl;
				cout << "2. Them cuoi danh sach" << endl;
				cout << "3. Them vao vi tri bat ky" << endl;
				cout << "0. Tro ve Main menu" << endl;
				cout << "Nhap vao lua chon: ";
				cin >> choice2;
				cin.ignore();
				switch (choice2)
				{
				case 1:
				{
					p.ThemDau(1);
					cout << "Nhap 1 de tro lai menu tao, 0 de tro lai menu chinh" << endl;
					cout << "Nhap lua chon :";
					cin >> choice3;
					while (choice3 != 0 && choice3 != 1)
					{
						cout << "Ban phai nhap 0 de tro lai Main menu, nhap 1 de tro lai menu tao " << endl;
						cout << "Nhap lai: ";
						cin >> choice3;
					}
					break;
				}
				case 2:
				{
					int n = getSize(p);
					p.ThemDau(n + 1);
					cout << "Nhap 1 de tro lai menu tao, 0 de tro lai menu chinh" << endl;
					cout << "Nhap lua chon :";
					cin >> choice3;
					while (choice3 != 0 && choice3 != 1)
					{
						cout << "Ban phai nhap 0 de tro lai Main menu, nhap 1 de tro lai menu tao " << endl;
						cout << "Nhap lai: ";
						cin >> choice3;
					}
					break;
				}
				case 3:
				{
					int k, n = getSize(p);
					cout << "Nhap vao vi tri ban can them:";
					cin >> k;
					while (k < 1 || k > n)
					{
						cout << "Vi tri nhap khong ton tai" << endl;
						cout << "Nhap lai: ";
						cin >> k;
					};
					p.ThemDau(k);
					cout << "Nhap 1 de tro lai menu tao, 0 de tro lai menu chinh" << endl;
					cout << "Nhap lua chon :";
					cin >> choice3;
					while (choice3 != 0 && choice3 != 1)
					{
						cout << "Ban phai nhap 0 de tro lai Main menu, nhap 1 de tro lai menu tao " << endl;
						cout << "Nhap lai: ";
						cin >> choice3;
					}
					break;
				}
				case 0:
					choice3 = 0;
					break;
				default:
					break;
				}
			} while (choice3 != 0);
			break;
		case 3:
			system("cls");
			do {
				system("cls");
				cout << "------- Menu xoa --------" << endl;
				cout << "1. Xoa dau danh sach" << endl;
				cout << "2. Xoa cuoi danh sach" << endl;
				cout << "3. Xoa vao vi tri bat ky" << endl;
				cout << "0. Tro ve Main menu" << endl;
				cout << "Nhap vao lua chon: ";
				cin >> choice2;
				cin.ignore();
				switch (choice2)
				{
				case 1:
				{
					p.XoaDau(1);
					cout << "Nhap 1 de tro lai menu xoa, 0 de tro lai menu chinh" << endl;
					cout << "Nhap lua chon :";
					cin >> choice3;
					while (choice3 != 0 && choice3 != 1)
					{
						cout << "Ban phai nhap 0 de tro lai Main menu, nhap 1 de tro lai menu xoa " << endl;
						cout << "Nhap lai: ";
						cin >> choice3;
					}
					break;
				}
				case 2:
				{
					int n = getSize(p);
					p.XoaDau(n + 1);
					cout << "Nhap 1 de tro lai menu xoa, 0 de tro lai menu chinh" << endl;
					cout << "Nhap lua chon :";
					cin >> choice3;
					while (choice3 != 0 && choice3 != 1)
					{
						cout << "Ban phai nhap 0 de tro lai Main menu, nhap 1 de tro lai menu xoa" << endl;
						cout << "Nhap lai: ";
						cin >> choice3;
					}
					break;
				}
				case 3:
				{
					int k, n = getSize(p);
					cout << "Nhap vao vi tri ban can xoa: ";
					cin >> k;
					while (k < 1 || k > n)
					{
						cout << "Vi tri nhap khong ton tai" << endl;
						cout << "Nhap lai: ";
						cin >> k;
					};
					p.XoaDau(k);
					cout << "Nhap 1 de tro lai menu xoa, 0 de tro lai menu chinh" << endl;
					cout << "Nhap lua chon :";
					cin >> choice3;
					while (choice3 != 0 && choice3 != 1)
					{
						cout << "Ban phai nhap 0 de tro lai Main menu, nhap 1 de tro lai menu xoa" << endl;
						cout << "Nhap lai: ";
						cin >> choice3;
					}
					break;
				}
				case 0:
					choice3 = 0;
					break;
				default:
					break;
				}
			} while (choice3 != 0);
			break;
		case 4: 
		{
			int k, n = getSize(p);
			cout << "Nhap vao vi tri ban can cap nhat: ";
			cin >> k;
			while (k < 1 || k > n)
			{
				cout << "Vi tri nhap khong ton tai" << endl;
				cout << "Nhap lai: ";
				cin >> k;
			};
			p.CapNhat(n);
			cout << "CAP NHAT THANH CONG" << endl;
			cout << "Bam 0 de tro lai Main menu " << endl;
			cin >> choice2;
			while (choice2 != 0)
			{
				cout << "Ban phai nhap 0 de tro lai Main menu " << endl;
				cout << "Nhap lai: ";
				cin >> choice2;
			}
			break;
		}
		case 5:
		{
			int n;
			cout << "Nhap vao ID :";
			cin >> n;
			p.TimKiem(n);
			cout << "Bam 0 de tro lai Main menu " << endl;
			cin >> choice2;
			while (choice2 != 0)
			{
				cout << "Ban phai nhap 0 de tro lai Main menu " << endl;
				cout << "Nhap lai: ";
				cin >> choice2;
			}
			break;
		}
		case 6:
			system("cls");
			do {
				cout << "------- Menu sap xep --------" << endl;
				cout << "1. Sap xep tang" << endl;
				cout << "2. Sap xep giam" << endl;
				cout << "0. Tro ve Main menu" << endl;
				cout << "Nhap vao lua chon: ";
				cin >> choice2;
				cin.ignore();
				switch (choice2)
				{
				case 1:
				{
					p.SapXep(tang);
					cout << "Nhap 1 de tro lai menu sap xep, 0 de tro lai menu chinh" << endl;
					cout << "Nhap lua chon :";
					cin >> choice3;
					while (choice3 != 0 && choice3 != 1)
					{
						cout << "Ban phai nhap 0 de tro lai Main menu, nhap 1 de tro lai menu sap xep" << endl;
						cout << "Nhap lai: ";
						cin >> choice3;
					}
					break;
				}
				case 2:
				{
					p.SapXep(giam);
					cout << "Nhap 1 de tro lai menu tao, 0 de tro lai menu chinh" << endl;
					cout << "Nhap lua chon :";
					cin >> choice3;
					while (choice3 != 0 && choice3 != 1)
					{
						cout << "Ban phai nhap 0 de tro lai Main menu, nhap 1 de tro lai menu tao " << endl;
						cout << "Nhap lai: ";
						cin >> choice3;
					}
					break;
				}
				case 0:
					choice3 = 0;
					break;
				default:
					break;
				}
			} while (choice3 != 0);
			break;
		default:
			break;
		}
	} while (choice1 != 0);
	return 0;
}
