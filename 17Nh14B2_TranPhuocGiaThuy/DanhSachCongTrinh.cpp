#include "DanhSachCongTrinh.h"

CongTrinhKhoaHoc TaoMoiCongTrinh()
{
	int mact, namxuatban, checkArea;
	string tenct;
	bool khuvuc;
	cout << "----- Nhap vao cong trinh khoa khoc -----" << endl;
	cout << "Nhap ma cong trinh: ";
	cin >> mact;
	cout << "Nhap nam xuat ban: ";
	cin >> namxuatban;
	cin.ignore();
	cout << "Nhap ten chuong trinh khoa hoc: ";
	getline(cin, tenct);
	cout << "Nhap khu vuc (1: Trong nuoc/ 0: Ngoai nuoc): ";
	cin >> checkArea;
	checkArea == 1 ? (khuvuc = true) : (khuvuc = false);
	return CongTrinhKhoaHoc(mact, namxuatban, tenct, khuvuc);
	cout << endl;
}

DanhSachCongTrinh::DanhSachCongTrinh(int n)
{
	this->size = n;
	this->ctkh = new CongTrinhKhoaHoc[this->size];
	this->ctkh[0] = CongTrinhKhoaHoc(1, 2019, "demo", false);
}

DanhSachCongTrinh::~DanhSachCongTrinh()
{
	delete[] this->ctkh;
}

void DanhSachCongTrinh::ThemDau(int pos)
{
	CongTrinhKhoaHoc c = TaoMoiCongTrinh();
	CongTrinhKhoaHoc* copy = new CongTrinhKhoaHoc[this->size];
	for (int i = 0; i < this->size ; i++)
	{
		copy[i] = this->ctkh[i];
	}
	delete[] this->ctkh;
	this->size++;
	this->ctkh = new CongTrinhKhoaHoc[this->size];
	for (int j = 0; j < this->size; j++)
	{
		if (j < pos - 1)
		{
			*(this->ctkh + j) = *(copy + j);
		}
		else if (j > pos - 1)
		{
			*(this->ctkh + j) = *(copy + j - 1);
		}
		else
		{
			*(this->ctkh + j) = c;
		}
	}
	delete[] copy;
}

void DanhSachCongTrinh::XoaDau(int pos)
{
	CongTrinhKhoaHoc* copy = new CongTrinhKhoaHoc[this->size];
	for (int i = 0; i < this->size; i++)
	{
		copy[i] = this->ctkh[i];
	}
	delete[] this->ctkh;
	this->size--;
	this->ctkh = new CongTrinhKhoaHoc[this->size];
	for (int j = 0; j < this->size; j++)
	{
		if (j < pos - 1)
		{
			this->ctkh[j] = copy[j];
		}
		else
		{
			this->ctkh[j] = copy[j + 1];
		}
	}
	delete[] copy;
}

void DanhSachCongTrinh::CapNhat(int pos)
{
	this->ctkh[pos - 1].Show();
	cout << "Moi ban nhap thong tin muon cap nhat: ";
	CongTrinhKhoaHoc c = TaoMoiCongTrinh();
	this->ctkh[pos - 1] = c;
	cout << "Cong trinh sau khi cap nhat: ";
	this->ctkh[pos - 1].Show();
}

void DanhSachCongTrinh::TimKiem(int id)
{
	int count, check = 0;
	for (int i = 0; i < this->size; i++)
	{
		if (getID(this->ctkh[i]) == id)
		{
			count = i;
			check = 1;
			break;
		}
	}
	if (check == 0)
	{
		cout << "KHONG TIM THAY CHUONG TRINH" << endl;
	}
	else {
		cout << "Chuong trinh duoc tim thay tai vi tri " << count + 1 << endl;
		this->ctkh[count].Show();
	}
}

void DanhSachCongTrinh::SapXep(bool Func(int,int))
{
	CongTrinhKhoaHoc* copy = new CongTrinhKhoaHoc[this->size];
	for (int i = 0; i < this->size; i++)
	{
		*(copy + i) = *(this->ctkh + i);
	}
	for (int i = 0; i < this->size - 1; i++)
	{
		for (int j = this->size - 1; j > i; j--)
		{
			if (Func(getID(copy[j]), getID(copy[j - 1])))
			{
				Swap(copy[j], copy[j - 1]);
			}
		}
	}
	for (int k = 0; k < this->size; k++)
	{
		cout << "----- DANH SACH SAU KHI SAP XEP -----" << endl;
		copy[k].Show();
	}
	delete[] copy;
}

void DanhSachCongTrinh::Swap(CongTrinhKhoaHoc& a, CongTrinhKhoaHoc& b)
{
	CongTrinhKhoaHoc temp;
	temp = a;
	a = b;
	b = temp;
}

void DanhSachCongTrinh::Display()
{
	if (this->size != 0) {
		cout << "--------DANH SACH CONG TRINH KHOA HOC" << endl;
		for (int i = 0; i < this->size; i++)
		{
			this->ctkh[i].Show();
		}
		cout << endl;
	}
	else {
		cout << "LIST IS EMPTY" << endl;
	}
}

int getSize(const DanhSachCongTrinh& p)
{
	return p.size;
}
