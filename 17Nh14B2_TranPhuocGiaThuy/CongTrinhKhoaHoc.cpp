#include "CongTrinhKhoaHoc.h"

CongTrinhKhoaHoc::CongTrinhKhoaHoc()
{
}

CongTrinhKhoaHoc::CongTrinhKhoaHoc(int mact, int namxuatban, string tenct, bool khuvuc)
{
	this->MaCT = mact;
	this->NamXuatBan = namxuatban;
	this->TenCTKH = tenct;
	this->KhuVuc = khuvuc;
}

CongTrinhKhoaHoc::CongTrinhKhoaHoc(const CongTrinhKhoaHoc& c)
{
	this->MaCT = c.MaCT;
	this->NamXuatBan = c.NamXuatBan;
	this->TenCTKH = c.TenCTKH;
	this->KhuVuc = c.KhuVuc;
}

CongTrinhKhoaHoc::~CongTrinhKhoaHoc()
{
	
}

void CongTrinhKhoaHoc::Show()
{
	cout << "----- Thong tin chuong trinh khoa hoc -----" << endl;
	cout << "Ma chuong trinh khoa hoc: " << this->MaCT << endl;
	cout << "Nam xuat ban: " << this->NamXuatBan << endl;
	cout << "Ten chuong trinh khoa hoc: " << this->TenCTKH << endl;
	cout << "Khu Vuc: ";
	this->KhuVuc == false ? (cout << "Ngoai nuoc") : (cout << "Trong nuoc");
	cout << endl;
}


int getID(const CongTrinhKhoaHoc& c)
{
	return c.MaCT;
}
