#pragma once
#include "CongTrinhKhoaHoc.h"
class DanhSachCongTrinh
{
private:
	CongTrinhKhoaHoc* ctkh;
	int size;
public:
	DanhSachCongTrinh(int = 1);
	DanhSachCongTrinh(const DanhSachCongTrinh&);
	~DanhSachCongTrinh();
	//constructor

	void Swap(CongTrinhKhoaHoc&, CongTrinhKhoaHoc&);

	void ThemDau(int);

	void XoaDau(int);
	
	void CapNhat(int);

	void TimKiem(int);

	void SapXep(bool (*Func)(int, int));

	void Display();

	friend int getSize(const DanhSachCongTrinh&);
};

